// unorder_multiset.cpp 

#include <iostream>
#include <string>
#include <unordered_set>

using namespace std;

int main(){
    unordered_set<string> names { "Meher", "Krishna", "Patel", "Meher"};
    unordered_multiset<string> names_mul { "Meher", "Krishna", "Patel", "Meher"};
    
    names.insert("mekrip"); // insert new name

    cout << "set" << endl; // set
    for (string name : names)
        cout << name << " ";
    cout << endl << endl;

    cout << "multiset" << endl; // multiset
    for (string name : names_mul)
        cout << name << " ";
    cout << endl;

    return 0;
}
