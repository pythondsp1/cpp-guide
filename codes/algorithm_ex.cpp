// algorithm_ex.cpp

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main(){
    vector<int> vec = { 10, 12, 3, 4, 15};
    
    // min_pos is the iterator
    vector<int>::iterator min_pos = min_element( vec.begin(), vec.end() );
    cout << "minimum value: " << *min_pos << endl;

    // 'auto' is easy to use
    auto max_pos = max_element( vec.begin(), vec.end() ); 
    cout << "maximum value: " << *max_pos << endl;
    
    sort( vec.begin(), vec.end());
    cout << "minimum value: " << *vec.begin() << endl << endl;

    cout << "print the value of vector" << endl;
    for (int v : vec)
        cout << v << " ";
    cout << endl << endl;

    // find the position of first 10 
    vector<int>::iterator pos_10 = find( vec.begin(), vec.end(), 10 );  

    // reverse the elements from the element 10
    reverse( pos_10, vec.end());
    // print values 
    cout << "print the value of vector (after reverse)" << endl;
    for (int v : vec)
        cout << v << " ";
    cout << endl << endl;
    
    // note that pos_10 is still the 3rd position
    // maximum value - exclude the 3rd position
    cout << "maximum value (exclude 3rd position): " 
        << *max_element( vec.begin(), pos_10) << endl;

    // maximum value - including the 3rd position
    cout << "maximum value (include 3rd position): "
       << *max_element( vec.begin(), ++pos_10) << endl;

    return 0;
}
