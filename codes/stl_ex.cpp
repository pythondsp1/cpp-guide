// stl_ex.cpp

#include <iostream>
#include <vector>

int main() {
    std::vector<int> v_int; // container : vector of type 'int'
    // std::vector<float> vf; // vector of type 'float'
    int i = 0;

    // print the size of vector, 
    std::cout << "vector size: " << v_int.size() << std::endl;
     
    for (int i=0; i<5; i++) // int i : local 'i' 
        v_int.push_back(i); 
    std::cout << "vector size: " << v_int.size() << std::endl;

    // iterator
    std::vector<int>::iterator vi = v_int.begin();
    while (vi != v_int.end()) {
        std::cout << "v_int[" << i << "]: " << *vi << std::endl;
        vi++;
        i++;
    }

    return 0;
}
