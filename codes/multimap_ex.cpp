// multimap_ex.cpp

#include <iostream>
#include <string>
#include <map>

using namespace std;

int main(){
    map<int, string> ranks;
    multimap<int, string> ranks_mul;

    ranks = { 
        {2, "Meher"},
        {3, "Patel"},
        {1, "Krishna"},
        {1, "Mekrip"}, // duplicate key will be igonred
        {4, "Krishna"}
    };

    // map
    cout << "map" << endl;
    for (auto rank : ranks)
        // first : key,   second : value
        cout << rank.first << ": " << rank.second << endl;
    cout << endl;

    // multimap
    ranks_mul = { 
        {2, "Meher"},
        {3, "Patel"},
        {1, "Krishna"},
        {1, "Mekrip"}, // duplicate key will be igonred
        {4, "Krishna"}
    };


    cout << "multimap" << endl;
    for (auto rank : ranks_mul)
        // first : key,   second : value
        cout << rank.first << ": " << rank.second << endl;
    cout << endl;

    return 0;
}
                

