.. _`ch_CppIntro`:

Introduction to C++
*******************

.. raw:: latex

    \chapterquote{When a man is, through his desires, confronted with great suffering , he understands their true nature; so when such suffering comes, it should be welcome. Suffering may come in order to eliminate further suffering. Suffering has to come, when it is of use in purging the soul from it's desires. }{Meher Baba}


Introduction
============

In previous chapters, we learn various features of C programming language. Now, we will discuss the C++ programming language, which can be considered as the superset of C. In the other words, all the codes in the previous chapters can be compiled directly using C++ compiler (i.e. using **g++**, instead of **gcc**), but vice-versa is not possible, as we saw in :numref:`c_ElseIfEx`, where 'or' keyword was used, and the code was compiled using 'g++'. 

The major difference between these two languages is that the C language is 'procedural programming language' whereas the C++ language is the **combination** of 'procedural programming' and 'object oriented programming (OOP)'. In this chapter, we will re-implement the previous codes using standard 'C++ headers' (instead of using 'C headers' in C++), which show the differences in 'procedural programming' using these two languages. Then OOP features of C++ will be discussed in next chapter.  Lastly, the C codes are saved with extension '.c', whereas C++ codes are saved with extension '.cpp' or '.cc'. We will use '.cpp' extension in this tutorial. 

Writing first code
==================

:numref:`cpp_helloWorld` shows the source code to print the 'Hello World' on the screen. To execute the code in the listing, open the terminal and go to the folder where program is saved and type following commands; which will generate the output on the terminal. 

.. code-block:: shell

    $ g++ -o out helloWorld.cpp
    $ ./out                       (in Unix)
    $ out                         (in Windows)

**Explanation** :numref:`cpp_helloWorld`

    C++ is also **case-sensitive** language. Similar to C language, each program contains one and only one  'main' function, which is the starting point of the code. In this listing, Line 6 has the 'main' function, which contains two other keywords i.e. 'int' and 'void'; here 'void' indicates that, the main 'function' does not have any input parameter, whereas 'int' represents that the return type of this function is 'integer'.
    
    Note that, in C++ the 'iostream' is the header file (instead of 'stdio.h'), which contains all the input and output formats i.e. 'cin' and 'cout' respectively. The function of 'cout' and 'cin' is similar to 'printf' and 'scanf' statements in C. Only these statements are needed to be changed in the previous chapters to convert the C codes into C++, which is discussed in this chapter. Also, the complete list of C++ keywords are shown in :numref:`tbl_KeywordsC`. 
        
    In Line 9, keyword 'cout' is used, which prints the characters on the screen. Next 'namespace std::' is used along with the 'cout' because 'cout' is defined in this namespace. Further, 'std' is defined in 'iostream header file' therefore it is included at Line 4. Then, 'endl' is used to terminate the line, which is also defined in the 'namespace std'. Lastly '\<\<'' is known as 'stream insertion operator' which inserts the values to output stream i.e. 'Hello', 'end of line (i.e. line-change)' and 'World' will be inserted to output stream. Hence, 'Hello' will be displayed at first line and 'World' will be displayed at second line in the output, as shown in Lines 14-15. 
    
    Similar to C,  multiple lines can be commented by using '/ \*' and '\* /' as shown in Lines 13-16; whereas '//' is used for single line comments. The comments make code more readable; e.g. in this listing, functions of all the lines are described as comments.

.. literalinclude:: codes/Introduction-to-C++/helloWorld.cpp
    :language: c++
    :linenos:
    :caption: Print Hello World
    :name: cpp_helloWorld

'Namespaces' and 'Line termination'
===================================

In :numref:`cpp_helloWorld`, we used 'std::' at various places. To avoid writing the 'std::', we can define the  namespace at the top of the code with keywords 'using namespace', as shown in :numref:`cpp_helloWorldNamespace`. Further, '\ n' is used for ending the line at Line 10 (instead of endl) .

.. literalinclude:: codes/Introduction-to-C++/helloWorldNamespace.cpp
    :language: c++
    :linenos:
    :caption: Using namespace
    :name: cpp_helloWorldNamespace


**Note that, Line 9 can be written in multiple lines as well as shown in Lines 11-13 (uncommet these lines to check the outputs). In the other words, it is the ';' which terminates the line, not the white spaces.**

C to C++ conversion examples
============================

In this section, some of the previous codes are re-written using C++. Here, we can see that, to convert the C code to C++, we need to change the header file i.e. **replace 'stdio.h' with 'iostream'; and then substitute all the 'printf' and 'scanf' statements with 'cout' and 'cin' statements respectively**. 

'cin' and 'cout'
----------------

In :numref:`cpp_cinEx`, the name is provide by the user (Line 11) and then message is displayed through Line 13, which is similar to :numref:`c_scanfEx`.

.. literalinclude:: codes/Introduction-to-C++/cinEx.cpp
    :language: c++
    :linenos:
    :caption: 'cin' and 'cout'
    :name: cpp_cinEx

Dot product
-----------

:numref:`cpp_dot_product` performed the dot product of two vectors which is discussed in :numref:`c_dot_product`.

.. literalinclude:: codes/Introduction-to-C++/dot_product.cpp
    :language: c++
    :linenos:
    :caption: Dot product of vectors
    :name: cpp_dot_product

Bitwise operators
-----------------

:numref:`cpp_shiftOpEx` shows the bitwise operations, which is discussed in :numref:`c_shiftOpEx`.

.. literalinclude:: codes/Introduction-to-C++/shiftOpEx.cpp
    :language: c++
    :linenos:
    :caption: Bitwise operators
    :name: cpp_shiftOpEx


Formatted input and outputs
===========================

Similar to :numref:`<Chapter %s <ch_FormatIO>`, in this section we will see various formatting commands for inputs and outputs in C++. 

Integer values in Binary, Hex, Octal \& Decimal formats
-------------------------------------------------------


In this section, we will learn to print the **integer values** in different formats i.e. Binary, Hexadecimal, Octal and Decimal. This can be done using keywords 'bitset', 'hex', 'oct' and 'dec' as shown in :numref:`cpp_hexDec`. For further understanding,  see the comments and outputs of the code in the listing. **Note that, the variable 'h' is defined as 'hexadecimal' and when it is printed at Line 28, the default output is in 'octal format'; and when again it is printed at Line 32, the default output is in 'decimal format'.** Therefore, it is better to define the print-format for hexadecimal and octal values.  

.. literalinclude:: codes/Introduction-to-C++/hexDec.cpp
    :language: c++
    :linenos:
    :caption: Printing numbers in different formats
    :name: cpp_hexDec


Floating point values in Scientific and Fixed notations
-------------------------------------------------------

Floating point values can be represented in Scientific and Fixed point notation using keywords '**scientific**' and '**fixed**' respectively, as shown in :numref:`cpp_floatFormat`. Also, we can set the precision for the outputs using '**setprecision}' as shown in Line 17 of the listing. **Also, note the ambiguities between the actual value of 'a' and all the results**.  

.. literalinclude:: codes/Introduction-to-C++/floatFormat.cpp
    :language: c++
    :linenos:
    :caption: Floating point values in Scientific and Fixed notations
    :name: cpp_floatFormat


Modify 'cout' format
--------------------

The 'setw(10)' is used for setting the minimum width as 10 for the outputs, which is similar to '\%10d' in C. Similarly, 'left' and 'right' keywords are used to left-justify and right-justify the outputs, as shown in :numref:`cpp_coutFormat`. 

.. literalinclude:: codes/Introduction-to-C++/coutFormat.cpp
    :language: c++
    :linenos:
    :caption: Modify 'cout' format
    :name: cpp_coutFormat


'cin' formats
-------------

The **hex** and **oct** keywords can be used with 'cin' statement to read the Hexadecimal and Octal values respectively, as shown in :numref:`cpp_cinFormats`. 

.. literalinclude:: codes/Introduction-to-C++/cinFormats.cpp
    :language: c++
    :linenos:
    :caption: 'cin' formats
    :name: cpp_cinFormats


Type 'string'
=============

C++ provide the 'std::string' type to handle strings. :numref:`c_stringInitSolution` is re-implemented using 'string-type' in :numref:`cpp_stringType`. Please note the following things about string, 

* Size of 'string' is 4 (see output of Line 19); whereas the size of 'char' is 1 byte.
* Value to string can be assigned using 'cin' (Line 16). And similar to char, the input will be read till first 'space' i.e. input 'Meher Krishna' will be read as 'Meher' (see output at Line 60 and 64). 
* 'string' can be passed to function as 'const' as well (Line 28); which can not be modified by the function i.e. Line 36 will generate error. 
* Note that, string 'b' contains '7' characters, and Lines 40-44 assigns 10 'x' to 'b'. If we look the output of Line 55, then we find that there are only 7 'x' (i.e. equal to size defined at Line 13). In the other word, string size can not be modified after initialization or after getting the input from user. 
* If we try to print the string values above it's size (i.e. 7 here), then garbage values will be printed (see output of Line 55). 
* Lastly, similar to :numref:`c_stringInitSolution`,  it better to use 'Null character $(i.e. \backslash 0)$' in the 'for loop' to access the elements of the string (uncomment Lines 47-52) instead of Lines 39-45. 

.. literalinclude:: codes/Introduction-to-C++/stringType.cpp
    :language: c++
    :linenos:
    :caption: Type 'string'
    :name: cpp_stringType

Scope resolution operator '::'
==============================

Scope resolution operation can be used when the namespace-scope or global-variable-scope is hidden in a function due to same name, as shown in :numref:`cpp_scopeResolutionEx`. Here, local variable 'a' is defined at Line 10, which hides the access to global variable (due to same name). Now, '::' should be used to access the global variable as shown in Line 15.  

.. literalinclude:: codes/Introduction-to-C++/scopeResolutionEx.cpp
    :language: c++
    :linenos:
    :caption: Scope resolution operator
    :name: cpp_scopeResolutionEx


Function overloading
====================

C++ allows the function overloading, which is not allowed in C. In function overloading, two functions can have same; and the correct function is called based on the **types of the input-parameters**, as shown in :numref:`cpp_functionOverload`. In the listing, the function 'add2Num' is used 4 times with different input-parameters. Since Line 43 contains inputs of type 'integer' therefore function at Line 11 will be invoked. In the same way other functions will be invoked based on input types as shown in the listing. 
 
.. literalinclude:: codes/Introduction-to-C++/functionOverload.cpp
    :language: c++
    :linenos:
    :caption: Function overloading
    :name: cpp_functionOverload



Namespaces
==========


.. literalinclude:: codes/Introduction-to-C++/namespaceEx.cpp
    :language: c++
    :linenos:
    :caption: Namespaces
    :name: cpp_namespaceEx


.. literalinclude:: codes/Introduction-to-C++/namespaceEx2.cpp
    :language: c++
    :linenos:
    :caption: Namespace inside Namespace
    :name: cpp_namespaceEx2

Conclusion
==========

In this chapter, we wrote simple 'Hello World' programs, which illustrate the concept of 'main function', 'name space' and 'printing output on screen'. Further, we converted some of the previous C codes into C++ codes. Also, various input and output formats are discussed. Lastly, strings, function overloading and scope resolution operators are also discussed. 

