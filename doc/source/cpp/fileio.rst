.. _`ch_fileio`: 

File operations in C
********************

.. raw:: latex

    \chapterquote{But I say to you who listen: Love your enemies, do what is good to those who hate you, bless those who curse you, pray for those who mistreat you.}{Jesus Christ}

Introduction
============

In previous chapters, we used variable, arrays and structures to store the data; but such data do not store permanently and lost after the termination of the program. Data can be stored to file using keyword 'fwrite'; and can be read from the file using keyword 'fscanf', as shown in this chapter. 


Write data to file
==================

For writing or reading data from a file, we need to create one 'FILE pointer' as shown in Line 10 of :numref:`c_writeDataEx`. Next, we need to open the file, which is done at Line 13; this line prints the message at Line 14, if the file can not be open. Note that, the file 'data.txt' file is saved inside the 'data' folder, therefore we need to create the 'data' folder first, as compiler can not create the folder. If file is open successfully, the compiler will reach to 'else' block at Line 16; where it will read the data from the terminal (Line 19), and then save it to file using 'fprintf' command at Line 22. The 'while loop' is used at Line 21, which reads the data from terminal until 'end of file command' is given i.e. 'ctrl-Z' or 'crtl-C', as shown in :numref:`fig_writeDataEx`. The data saved by the listing is shown in :numref:`fig_writeDataEx2`. 


.. literalinclude:: codes/FileIO/writeDataEx.c
    :language: c++
    :linenos:
    :caption: Write data to file
    :name: c_writeDataEx

.. _`fig_writeDataEx`:

.. figure:: fig/FileIO/writeDataEx.jpg

   Reading data from terminal


.. _`fig_writeDataEx2`:

.. figure:: fig/FileIO/writeDataEx2.jpg

   Data in 'data.txt' file


Read data from file
===================

Reading operation is similar to writing operation. The 'fscanf' command is used to read the data from the file, as shown in Line 16 and 20 of :numref:`c_readDataEx`. 

.. literalinclude:: codes/FileIO/readDataEx.c
    :language: c++
    :linenos:
    :caption: Read data from file
    :name: c_readDataEx


Conclusion
==========

In this chapter, we learn to read and write data to file. We can permanently store the data to file using 'fwrite command'; and then this data can be processed and analyzed by some other software like Python and R etc. 