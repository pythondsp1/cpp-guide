.. _`ch_MultiFile`:

Separating codes in multiple files
**********************************

.. raw:: latex

    \chapterquote{The only way of not being upset by the blame is to be detached from the praise also; it is only through complete detachment that a person  can keep unmoved by the opposites of praise and blame.}{Meher Baba}


Introduction
============

In this chapter, we will learn to split the program in multiple files. In this way, the code-files are more manageable. 

Program to separate
===================


:numref:`c_main-to-split` is the code, which we want to write in separate files. In this code, value of constant PI is printed at Line 18. Loops at Line 20 calculate the sum of all elements of array 'global\_array'. Also, two functions are defined i.e. 'add2Num' and 'diff2Num' to calculate the sum and difference of two numbers. 

.. literalinclude:: codes/Separating-codes-in-multiple-files/main-to-split.c
    :language: c++
    :linenos:
    :caption: Program to separate in multiple files 
    :name: c_main-to-split


Separating the main function
============================

In this section, we will write the main function in one file (:numref:`c_main1-split`), constants in second file (:numref:`c_my_constants`) and rest of the code in the third file (:numref:`c_my_header`). Since, we have splited the code in two different files, therefore we need to include these files in the main project as shown in Lines 5-6 of :numref:`c_main1-split`. 

.. important:: 

    Note that, each file contains 'stdio.h' at the top which is enclosed between <...>, whereas files e.g. 'my\_header.h' is enclosed between ''...''. 

    When we use ''...'', then compiler look for the file in the current project director, whereas <...> looks for the files in the standard library locations.

.. literalinclude:: codes/Separating-codes-in-multiple-files/main1.c
    :language: c++
    :linenos:
    :caption: Program to separate in multiple files 
    :name: c_main1-split

.. literalinclude:: codes/Separating-codes-in-multiple-files/my_constants.c
    :language: c++
    :linenos:
    :caption: Contants and global variable in 'my\_constants.c' file 
    :name: c_my_constants

.. literalinclude:: codes/Separating-codes-in-multiple-files/my_header.h
    :language: c++
    :linenos:
    :caption: Functions prototype and definition 
    :name: c_my_header


Separating function-prototype and function-definition
=====================================================

In this section, we will further split the file 'my\_header.h' in :numref:`c_my_header`, as shown in :numref:`c_my_func_def` and :numref:`c_my_header2`. Following two points are important here, 


* If we uncomment the Line 4 of :numref:`c_my_header2`, then we can compile the :numref:`c_main2-split` by using command 'g++ main2.c -o out' as the 'my\_func\_def.c' file is included to main2.c via 'my\_header2.h'; but this is not the case if we comment this Line. 

* If Line 4 of :numref:`c_my_header2` is commented, then there is no way to compile it directly using command in above point. We need to separately compile it using 'g++ main2.c my\_func\_def.c -o out'. Now, the code will compile successfully. 

.. literalinclude:: codes/Separating-codes-in-multiple-files/my_func_def.c
    :language: c++
    :linenos:
    :caption: Functions prototypes 
    :name: c_my_func_def


.. literalinclude:: codes/Separating-codes-in-multiple-files/my_header2.h
    :language: c++
    :linenos:
    :caption: Functions definition 
    :name: c_my_header2


.. literalinclude:: codes/Separating-codes-in-multiple-files/main2.c
    :language: c++
    :linenos:
    :caption: Main function
    :name: c_main2-split

Conclusion
==========

In this chapter, we learn to separate the program into multiple files. Also, we saw that we need to compile multiple files, for the cases where function-definitions-file is not included to function-header-file (i.e. file that contains function prototypes). 

