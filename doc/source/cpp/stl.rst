Standard Template Library (STL)
*******************************

Understanding templates
=======================

The STL is the collection of **C++ templates**. Therefore, let's understand the template with an example first. 

In :numref:`cpp_square_ex`, the square of 'int' and 'double' is calculated at Lines 7 and 11. Note that, the two functions are exactly same except the 'types' of the input and the output. This repetition of the code can be removed using templates as shown in :numref:`cpp_template_ex`. 

.. code-block:: c++
    :linenos:
    :caption: Square of 'int' and 'double'
    :name: cpp_square_ex

    // square_ex.cpp

    #include <iostream>

    using namespace std;

    int square(int x){
        return x*x;
    }

    double square(double x){
        return x*x;
    }


    int main(){
        int a = 2;
        double b = 2.5;

        cout << square(a) << endl; // invoke : int square(int)
        cout << square(b) << endl; // invoke : float square(float)

        return 0;
    }
        

Below is the output of the above code, 

.. code-block:: shell

    $ g++ -o out square_ex.cpp 
    $ ./out
    4
    6.25



:numref:`cpp_template_ex` is the function template, which can calculate the square of both 'int' and 'double' types. 

.. code-block:: c++
    :caption: Function template for calculating square of 'int' and 'double'
    :name: cpp_template_ex
    
    // template_ex.cpp

    #include <iostream>

    using namespace std;


    template <typename T>
    T square(T x){
        return x*x;
    }

    int main(){
        int a = 2;
        double b = 2.5;

        cout << square<int>(a) << endl;  // or use 'square(a)' without 'int'
        cout << square<float>(b) << endl; // or use 'square(a)' without 'double'

        return 0;
    }
        

.. note::

    For 'function template', we can use square(a) instead of square<int>(a); but for the 'class template' we need to define the type explicitly. 

Below is the output of the above code, 

.. code-block:: shell

    $ g++ -o out template_ex.cpp 
    $ ./out
    4
    6.25

Standard Template Library
=========================

The STL (Standard Template Library) is a collection of **C++ templates** which provides various useful implementations of algorithms and data structures such as 'vector', 'queue', and 'list' etc. Also, these templates are 'standardized', efficient', 'accurate' and allows 're-usability'. The templates can be divided into three categories, 

#. **Containers**: The containers are used to manage the collection of objects e.g. vector, lists and map etc. 

#. **Iterators**: The iterators are used to loop through the each element of the the containers.

#. **Algorithms**: The algorithms are used on containers to perform various operations such as sorting, searching and transforming etc. 


:numref:`c_stl_ex` is an example of 'container', 'iterator' and 'algorithm' from the STL library. Here an integer-container i.e. '**vector v_int**' is defined at Line 8. Then the '**push_back method**' is used at Line 16, which append the elements in the vector. Next, the variable '**vi**' of type '**iterator**' is used at Line 20, which stores the initial location of the container 'v_int'. This iterator 'vi' is used to print the values stored in the vector using 'while' loop at Lines 21-25. Finally, an **algorithm 'reverse'** is used at Line 28, which reverse the number stored in the **container 'v_int'** using **iterators 'begin()' and 'end()'**. Following functions are used in this listing, 

* **size()** : returns the size of of the vector.
* **begin()** : returns an iterator to the start of the vector.
* **end()** : returns an iterator to the end of the the vector.
* **push_back** : insert the value at the end of the vector.
* **reverse**: algorithm which uses the the iterators. 

.. note::

    In :numref:`c_stl_ex`, the 'vectors' and 'iterators' are defined in the header file 'iterator.h'; whereas the algorithm 'reverse' is defined in the header file 'algorithm.h'. 

    The 'end()' iterator does not represent the position of the last element, but the position after the last element; hence '**!=**' is used at Line 21 of :numref:`c_stl_ex` (instead of '==' and '<=').

.. code-block:: c++
    :linenos:
    :emphasize-lines: 8, 16, 20, 28
    :caption: STL example
    :name: c_stl_ex

    // stl_ex.cpp

    #include <iostream>
    #include <vector>
    #include <algorithm>

    int main(){
        std::vector<int> v_int; // container : vector of type 'int'
        // std::vector<float> vf; // vector of type 'float'
        int i = 0;

        // print the size of vector,
        std::cout << "vector size: " << v_int.size() << std::endl;

        for (int i=0; i<5; i++) // int i : local 'i'
            v_int.push_back(i);
        std::cout << "vector size: " << v_int.size() << std::endl;

        // iterator v_int.begin() is stored in 'vi'
        std::vector<int>::iterator vi = v_int.begin();
        while (vi != v_int.end()) { // print using while loop
            std::cout << *vi << " ";
            vi++;
        }
        std::cout << std::endl;

        // reverse from iterator 'v_int.begin()' to iterator 'v_int.end()'
        reverse(v_int.begin(), v_int.end());
        std::cout << "reverse order" << std::endl;
        for (int i=0; i<v_int.size(); i++) // print using for loop
            std::cout << v_int[i] << " ";
        std::cout << std::endl;

        return 0;
    }


Below are the outputs for the above code, 

.. code-block:: text

    $ g++ stl_ex.cpp -o out
    $ ./out

    vector size: 0
    vector size: 5
    0 1 2 3 4 

    reverse order
    4 3 2 1 0 

.. note::

    Also we can replace the Line 31 with below line. The 'at()' function throws an exception when index is out of range. 

    .. code-block:: c++
    
        std::cout << v_int.at(i) << " ";


.. important:: 

    The aim in OOPs (object oriented programming) is to combine the 'data' and 'algorithms' together using 'objects' of the classes. Further the STL has three separate components i.e. 'Containers', 'Iterators' and 'Algorithms', which goes against the OOPs logic of combining the 'data' and the 'algorithms'. But this separation is quite useful, as it reduces the number of algorithm-implementations in STL by increasing the re-usability. In the other words, with the help of separation, an **algorithms** can be used for different **containers** (instead of one algorithm for each component). This is achieved by an intermediate entity i.e. **iterators**. 



Containers
==========

The containers can be divided into three categories i.e. 

* **Sequence container**: It is an ordered collection i.e. each element has unique position in the containers which depends on the 'time' and 'place' of insertion (independent of 'values'). STL contains five sequence containers, 

    - vector
    - array
    - deque
    - list
    - forward_list
    
* **Associative container**: It is a sorted collection where the position of the elements depends on the 'values' of the elements. STL has four associative containers, 

    - set
    - map
    - multiset
    - multimap
    
* **Unordered container**: In this container, the position of the elements do not matter i.e. if we put certain elements in an unordered container then their position may vary with time. STL has four types of unordered container, 

    - unordered_set
    - unordered_map
    - unordered_multiset
    - unordered_multimap


.. important:: 

    * Sequence containers are used to implement the arrays and link lists.
    * Associative containers are used to implement the binary trees. 
    * Unordered containers are used to implement the hash tables. 


Sequence containers
-------------------

In this section, we will see some examples of the sequence containers. 

Vector
^^^^^^

We already saw an example of vector in :numref:`c_stl_ex`, where we have stored the integers in the vector. 

A vector is a dynamic array, where we can directly access the elements using 'index'. Note that the insertion or elimination of the element from the 'end' is faster than the other location, as for the other location, the shifting of the elements are required to make a space for new value. below is the another example of vector where the square-roots of the numbers are stored in vector, 

.. code-block:: c++
    :linenos:
    :caption: Vectors
    :name: cpp_vector_ex

    // vector_ex.cpp

    #include <iostream>
    #include <vector>
    #include <cmath>

    using namespace std;

    int main(){
        vector<double> vec;  // by default : size = 0

        // append elements
        for (int i=0; i<4; i++)
            vec.push_back(sqrt(i)); // sqrt(i)

        // print elements
        for (int i=0; i<vec.size(); i++)
           cout << vec[i] << endl;

       return 0;
    } 
        

Below is the output of above code, 

.. code-block:: text

    $ g++ vector_ex.cpp -o out
    $ ./out
    0
    1
    1.41421
    1.73205


Array
^^^^^

The size of the vector changes, when we insert or delete an element to it. On the other hand, the 'arrays' have the fix sizes and we can only modify the 'values' of the elements in the array (not the size of array). 


.. code-block:: c++
    :linenos:
    :caption: Array
    :name: c_array_stl_ex

    // array_stl_ex.cpp

    #include <iostream>
    #include <array>

    using namespace std;

    int main(){
        array<int, 5> arr = {3, 4}; // int-array of size 5

        // print the values
        for (int i=0; i<arr.size(); i++)
            cout << i << ": " << arr[i] << endl;

        return 0;
    }
        

Below is the output of above code, 

.. note:: 

    Arrays are available in ISO C++ 2011 standard, therefore -std=c++11 is used for compilation.


.. code-block:: text

    $ g++ -std=c++11 array_stl_ex.cpp -o out
    $ ./out
    0: 3
    1: 4
    2: 0
    3: 0
    4: 0





Deque (double ended queues)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The 'deque' is the dynamic array that can grow in both direction. Therefore the 'insertion' and 'deletion' is faster at both the ends. 

.. code-block:: c++
    :linenos:
    :caption: Deque
    :name: c_deque_ex

    // deque_ex.cpp

    # include <iostream>
    #include <deque>

    using namespace std; 

    int main(){
        deque<int> deq = {1, 2}; // initialized deque
        cout << "size: " << deq.size() << endl;
        
        deq.push_front(10); // insert at the front
        deq.push_back(20); // insert in the end

        for (int i=0; i<deq.size(); i++)
            cout << deq[i] << ", ";

        cout << endl;

        return 0;
    }


Below is the output of above code, 

.. code-block:: text

    $ g++ -std=c++11 deque_ex.cpp -o out
    $ ./out
    size: 2
    10, 1, 2, 20, 



List
^^^^

A list is implemented using 'double link list' of elements i.e. each element has it's own memory space. Hence the index-access is not possible in lists. Further, it does not provide a random access i.e. if we want to access the 4th element that we need to go through the 3rd element. Therefore, the access-speed is slower than the 'array' and 'vector'. For insertion and deletion, only two links needed to be changes (instead of shifting the whole array), therefore insertion and deletion is faster in lists. 

.. note:: 

    * Lists do not provide the index and random access. 
    * Access to the elements is slower that 'array' and 'vector'.
    * But, insertion and deletion is faster than the 'array' and 'vector'. 
    

.. code-block:: c++
    :linenos:
    :caption: List
    :name: c_list_stl_ex

    // list_stl_ex.cpp

    #include <iostream>
    #include <list>

    using namespace std;

    int main(){
        list<int> lst;
        
        for (int i=0; i<5; i++)
            lst.push_back(i*2);

        cout << "Print using for loop" << endl;
        for (int l : lst) // for l in list:
            cout << l << " "; // print(l)
        cout << endl;

        cout << "Print using while loop" << endl;
        while (! lst.empty()){
            cout << lst.front() << " "; // print first element
            lst.pop_front(); // remove the first element
        }
        cout << endl;

        // // index and random access is not allowed in list
        // for (int i=0; i<lst.size(); i++)
            // cout << lst[i];

        return 0;
    }

.. tip::

    * The 'for loop' at Line 15 is known as '**range-based for loop**', which can be used to loop through any kind of data. 
    * Further, we can use 'auto' in Line 15 (instead of int) , which can determine the type automatically, 

    .. code-block:: c++
    
        for (auto l : lst) // for l in list:
                    cout << l << " "; // print(l)

Below is the output of above code, 


.. code-block:: text

    $ g++ -std=c++11 -o out list_stl_ex.cpp 
    $ ./out
    Print using for loop
    0 2 4 6 8 
    Print using while loop
    0 2 4 6 8 


Forward list
^^^^^^^^^^^^

Forward list is the 'single link list', therefore 'push_back' operation will not work as show in Line 12 of Listing :numref:`c_forward_list_stl_ex`, 


.. code-block:: c++
    :linenos:
    :caption: Forward list
    :name: c_forward_list_stl_ex

    // forward_list_stl_ex.cpp 

    #include <iostream>
    #include <forward_list>

    using namespace std;

    int main(){
        forward_list<int> lst;
        
        for (int i=0; i<5; i++)
            // lst.push_back(i*2); // push_back will not work
            lst.push_front(i*2);

        cout << "Print using for loop" << endl;
        for (int l : lst) // for l in list:
            cout << l << " "; // print(l)
        cout << endl;

        cout << "Print using while loop" << endl;
        while (! lst.empty()){
            cout << lst.front() << " "; // print first element
            lst.pop_front(); // remove the first element
        }
        cout << endl;

        // // index and random access is not allowed in list
        // for (int i=0; i<lst.size(); i++)
            // cout << lst[i];

        return 0;
    }




Below is the output of above code, 

.. code-block:: text

    $ g++ -std=c++11 -o out forward_list_stl_ex.cpp 
    $ ./out
    Print using for loop
    8 6 4 2 0 
    Print using while loop
    8 6 4 2 0 


Associative containers
----------------------

Associative containers **sort** the elements automatically based on one of the two criterion i.e. 'values' and 'key-values'.

Since the items are sorted, therefore 'push_back' and 'push_front' operations are not allowed in associative containers. The 'insert()' operation can be used for inserting item in the containers. 

Set and multiset
^^^^^^^^^^^^^^^^

* **set**: a set is collection of unique-values. If the collection have multiple same values, then only one value will be stored and rest will be ignored. 
* **multiset**: same as set, but can have duplicate values. 

Below is an example of set and multiset, 

.. code-block:: c++
    :linenos:
    :caption: Set and multiset
    :name: c_multiset_ex

    // multiset_ex.cpp

    #include <iostream>
    #include <string>
    #include <set>

    using namespace std;

    int main(){
        set<string> names { "Meher", "Krishna", "Patel", "Meher"};
        multiset<string> names_mul { "Meher", "Krishna", "Patel", "Meher"};
        
        names.insert("mekrip"); // insert new name

        cout << "set" << endl; // set
        for (string name : names)
            cout << name << " ";
        cout << endl << endl;

        cout << "multiset" << endl; // multiset
        for (string name : names_mul)
            cout << name << " ";
        cout << endl;

        return 0;
    }
    

Below is the output of above code. Note that the 'set' contains "Meher" once only, whereas multiset contains it twice. 

.. note::

    Names are ordered, first based on 'lowercase/uppercase' and then by alphabets'.

.. code-block:: text

    $ g++ -std=c++11 -o out multiset_ex.cpp 
    $ ./out
    set
    Krishna Meher Patel mekrip 

    multiset
    Krishna Meher Meher Patel 


Map and multimap
^^^^^^^^^^^^^^^^

* **map**: a map is the collection of key-value pairs. The elements are sorted based on the keys (instead of values). It can be used as **associative arrays** which have non-integer index. 
* **multimap**: same as 'map', but elements with duplicate keys are allowed. This can be used as '**dictionary**'.

Below is an example of Map and multimap,

.. code-block:: c++
    :linenos:
    :caption: Map and multimap
    :name: c_multimap_ex

    // multimap_ex.cpp

    #include <iostream>
    #include <string>
    #include <map>

    using namespace std;

    int main(){
        map<int, string> ranks;
        multimap<int, string> ranks_mul;

        ranks = { 
            {2, "Meher"},
            {3, "Patel"},
            {1, "Krishna"},
            {1, "Mekrip"}, // duplicate key will be igonred
            {4, "Krishna"}
        };

        // map
        cout << "map" << endl;
        for (auto rank : ranks)
            // first : key,   second : value
            cout << rank.first << ": " << rank.second << endl;
        cout << endl;

        // multimap
        ranks_mul = { 
            {2, "Meher"},
            {3, "Patel"},
            {1, "Krishna"},
            {1, "Mekrip"}, // duplicate key will be igonred
            {4, "Krishna"}
        };


        cout << "multimap" << endl;
        for (auto rank : ranks_mul)
            // first : key,   second : value
            cout << rank.first << ": " << rank.second << endl;
        cout << endl;

        return 0;
    }
                    

Below is the output of above code. Note that the 'map' contains the key "1"  once only, whereas multimap contains it twice. 


.. code-block:: text

    $ g++ -std=c++11 -o out multimap_ex.cpp 
    $ ./out
    map
    1: Krishna
    2: Meher
    3: Patel
    4: Krishna

    multimap
    1: Krishna
    1: Mekrip
    2: Meher
    3: Patel
    4: Krishna


Unordered containers
--------------------

Unordered containers do not have any order. Rest of the working is same as 'associative containers', as show in below two examples, 

Unordered set and multiset
^^^^^^^^^^^^^^^^^^^^^^^^^^

 Below is an example of unordered set and multiset, 

.. code-block:: c++
    :linenos:
    :caption: Unordered set and multiset
    :name: c_unorder_multiset_ex

    // unorder_multiset.cpp 

    #include <iostream>
    #include <string>
    #include <unordered_set>

    using namespace std;

    int main(){
        unordered_set<string> names { "Meher", "Krishna", "Patel", "Meher"};
        unordered_multiset<string> names_mul { "Meher", "Krishna", "Patel", "Meher"};
        
        names.insert("mekrip"); // insert new name

        cout << "set" << endl; // set
        for (string name : names)
            cout << name << " ";
        cout << endl << endl;

        cout << "multiset" << endl; // multiset
        for (string name : names_mul)
            cout << name << " ";
        cout << endl;

        return 0;
    }


Below is the output of above code, 

.. code-block:: text

    $ g++ -std=c++11 -o out unorder_multiset.cpp 
    $ ./out
    set
    mekrip Patel Krishna Meher 

    multiset
    Patel Krishna Meher Meher 


Unordered map and multimap
^^^^^^^^^^^^^^^^^^^^^^^^^^

Below is an example of unordered map and multimap, 


.. code-block:: c++
    :linenos:
    :caption: Unordered map and multimap
    :name: c_unorder_multimap_ex

    // unorder_multimap.cpp

    #include <iostream>
    #include <string>
    #include <unordered_map>

    using namespace std;

    int main(){
        unordered_map<int, string> ranks;
        unordered_multimap<int, string> ranks_mul;

        ranks = { 
            {2, "Meher"},
            {3, "Patel"},
            {1, "Krishna"},
            {1, "Mekrip"}, // duplicate key will be igonred
            {4, "Krishna"}
        };

        // map
        cout << "map" << endl;
        for (auto rank : ranks)
            // first : key,   second : value
            cout << rank.first << ": " << rank.second << endl;
        cout << endl;

        // multimap
        ranks_mul = { 
            {2, "Meher"},
            {3, "Patel"},
            {1, "Krishna"},
            {1, "Mekrip"}, // duplicate key will be igonred
            {4, "Krishna"}
        };


        cout << "multimap" << endl;
        for (auto rank : ranks_mul)
            // first : key,   second : value
            cout << rank.first << ": " << rank.second << endl;
        cout << endl;

        return 0;
    }
                    

Below is the output of above code, 

.. code-block:: text

    $ g++ -std=c++11 -o out unorder_multimap.cpp 
    $ ./out
    map
    4: Krishna
    1: Krishna
    3: Patel
    2: Meher

    multimap
    4: Krishna
    1: Mekrip
    1: Krishna
    3: Patel
    2: Meher


Iterators
=========

All containers provide same set of iterators. The following two iterators are the most important iterators, 

* **begin()** : returns an iterator to the start of the vector.
* **end()** : returns an iterator to the end of the the vector.

.. warning::

    The 'end()' iterator does not represent the position of the last element, but the position after the last element; hence '**!=**' is used at Line 18 of :numref:`c_iterator_ex` (instead of '=='  and '<=').

We already see the example of iterator in :numref:`c_stl_ex` using 'for' loop using 'size()' option. In the below code, the :numref:`c_stl_ex` is reimplemented using 'for' loop with 'begin()' and 'end()' iterators, 

.. important:: 

    It is better to iterate using 'begin()' and 'end()' option than the 'size()' option as, 

    * 'begin()' and 'end()' method can be applied to all type of containers. 
    * 'begin()' and 'end()' method is faster than the 'size()' method.

    Also in 'C++ 11', we can use 'range based for loop' for iterations as shown in :numref:`c_list_stl_ex`. 

.. code-block:: c++
    :linenos:
    :emphasize-lines: 18
    :caption: Iterators 
    :name: c_iterator_ex

    // iterator_ex.cpp

    #include <iostream>
    #include <vector>

    using namespace std;

    int main() {
        vector<int> v_int; // container : vector of type 'int'
        int i = 0;

        for (int i=0; i<5; i++) // int i : local 'i' 
            v_int.push_back(i); 

        // iterator
        vector<int>::iterator vi;

        for (vi = v_int.begin(); vi != v_int.end(); ++vi)
           cout << *vi << " "; 

        cout << endl;

        return 0;
    }

.. note::

    We can use 'const_iterator' instead of 'iterator' at line 16, which provides **read only** access for the container's element. 

    .. code-block:: c++
    
        // iterator
        vector<int>::const_iterator vi;

        // below will be invalid
        // *vi = 3; // as it is read only


There are 5 types of iterators, 

* Random access iterator
* Bidirectional iterator
* Forward iterator
* Input iterator
* Output iterator


Random access iterator
----------------------

Random access iterators allows the random access of the elements in the containers. :numref:`cpp_random_iter` shows the example of a random access iterator. These iterators can be increment or decrement using mathematical operations (Lines 17 and 21). Also, these iterators can be compared with each others (Line 26). 

.. note:: 

    * Random access iterators are provided by '**vector**', '**deque**' and '**array**'. 
    * Pre-increment (Line 21) is faster than post-increment, as post-increment stores the value in the temporary variable before incrementing it. 


.. code-block:: c++
    :linenos:
    :caption: Random access iterator
    :name: cpp_random_iter

    // random_iter.cpp

    #include <iostream>
    #include <vector>
    #include <algorithm> // 'find' is algorithm

    using namespace std;

    int main(){
        vector<int> v_int = {12, 222, 11, 14, 25, 19}; // container

        vector<int>::iterator vi = v_int.begin(); // iterator  vi -> 12
        vector<int>::iterator vi2 = find(vi, v_int.end(), 222); // iterator  vi -> 222
        cout << *vi << endl; //

        // increment iterator by 3
        vi = vi + 3;  // vi -> 14
        cout << *vi << endl;

        // ++vi is faster than vi++
        ++vi; // vi -> 25 
        cout << *vi << endl;


        cout << *vi2 << endl; // vi2 -> 222
        if (vi > vi2) // true as vi is pointing to 25, which comes after 222
           vi2 = vi; // vi2 ->25
       cout << *vi2 << endl;

        return 0;
    }


Bidirectional iterator
----------------------


.. note:: 

    * Bidirectional iterators are provided by **list**, **set/multiset** and **map/multimap**. 
    * Bidirectional iterator can be increment and decremented using 'vi\+\+' and 'vi\-\-' etc. 
    * Increment and decrement is not allowed using mathematical operations i.e. 'vi + 3' is invalid. 


Forward iterator
----------------

.. note::

    * Forward iterators are provided by 'forward_list' only. 
    * These iterators can only be incremented (not decremented). 
    * Also, these can not be incremented by mathematical operations i.e. 'vi + 3' is invalid. 


Input iterator
--------------

Input iterators read and process the values while iterating **forward**. We can read the value using input iterators, but can not write the value. Note that, this iterator can not move backward. 

.. code-block:: c++

    int i = *itr;

Output iterator
---------------

Output iterators output the value while iterating **forward**. We can write the value to the output iterator but can not read the value. Note that, this iterator can not move backward. 

.. code-block:: c++

    int *itr = 20;

Iterator functions
==================

STL provides some functions as well for iterators as shown in Lines 17 and 21 of :numref:`cpp_itr_function`. 

.. note:: 

    The function 'advance (Line 17)' is quite useful for incrementing the 'non random access iterators'. 

.. code-block:: c++
    :linenos:
    :emphasize-lines: 17, 21
    :caption: Iterator functions
    :name: cpp_itr_function

    // itr_function.cpp

    #include <iostream>
    #include <vector>
    #include <algorithm>

    using namespace std;

    int main(){
        vector<int> v_int = {12, 222, 11, 14, 25, 19}; // container

        vector<int>::iterator vi = v_int.begin(), vi3; // iterator  vi -> 12
        vector<int>::iterator vi2 = find(vi, v_int.end(), 222); // iterator  vi -> 222
        cout << *vi << endl; //

        // increment iterator by 3; good for 'non random access iterator'
        advance(vi, 3); // vi -> 14
        cout << *vi << endl;

        // distance between two iterator
        cout << distance(vi, vi2) << endl; // -2


        return 0;
    }



Algorithms
==========

The algorithms are used on containers to perform various operations such as sorting, searching and transforming etc. as shown in this section. 

In the below code, the 'min_element', 'max_element', 'find', 'sort' and 'reverse' algorithms are used on a integer-vector, 

.. code-block:: c++
    :linenos:
    :emphasize-lines: 13, 17, 20, 29, 32, 42, 46
    :caption: Algorithms
    :name: c_algorithm_ex

    // algorithm_ex.cpp

    #include <iostream>
    #include <vector>
    #include <algorithm>

    using namespace std;

    int main(){
        vector<int> vec = { 10, 12, 3, 4, 15};
        
        // min_pos is the iterator
        vector<int>::iterator min_pos = min_element( vec.begin(), vec.end() );
        cout << "minimum value: " << *min_pos << endl;

        // 'auto' is easy to use
        auto max_pos = max_element( vec.begin(), vec.end() ); 
        cout << "maximum value: " << *max_pos << endl;
        
        sort( vec.begin(), vec.end());
        cout << "minimum value: " << *vec.begin() << endl << endl;

        cout << "print the value of vector" << endl;
        for (int v : vec)
            cout << v << " ";
        cout << endl << endl;

        // find the position of first 10 
        vector<int>::iterator pos_10 = find( vec.begin(), vec.end(), 10 );  

        // reverse the elements from the element 10
        reverse( pos_10, vec.end());
        // print values 
        cout << "print the value of vector (after reverse)" << endl;
        for (int v : vec)
            cout << v << " ";
        cout << endl << endl;
        
        // note that pos_10 is still the 3rd position
        // maximum value - exclude the 3rd position
        cout << "maximum value (exclude 3rd position): " 
            << *max_element( vec.begin(), pos_10) << endl;

        // maximum value - including the 3rd position
        cout << "maximum value (include 3rd position): "
           << *max_element( vec.begin(), ++pos_10) << endl;

        return 0;
    }


Below is the output of above code, 

.. code-block:: text

    $ g++ -std=c++11 -o out algorithm_ex.cpp 
    $ ./out 
    minimum value: 3
    maximum value: 15
    minimum value: 3

    print the value of vector
    3 4 10 12 15 

    print the value of vector (after reverse)
    3 4 15 12 10 

    maximum value (exclude 3rd position): 4
    maximum value (include 3rd position): 15



Tables
======

In previous sections, we saw that the functions may not available to all kinds of containers e.g. the function 'push_front' is available for 'deque' but not for 'vector'. 

In this section, few tables are added which show the various functions available for different types of container. 

.. note::

    Following notations are used in the tables, 

    * 'ctr' : container
    * 'itr' : iterator
    * 'pos' : position
    * 'val' : value
    * 'idx' : index
    * 'ctr.erase(itr1 [,itr2])' : parameters in the bracket (i.e. [, itr2]) are optional


Function common to all container
--------------------------------

:numref:`tbl_func_for_all_container` shows the functions which are common to all the containers. 

.. _`tbl_func_for_all_container`:

.. table:: Functions common to all types of container

    +--------------------------+------------------------------------------------------------------------+
    | Function                 | Description                                                            |
    +==========================+========================================================================+
    | ctr.begin()              | returns an iterator to the first element of container 'ctr'            |
    +--------------------------+------------------------------------------------------------------------+
    | ctr.end()                | returns an iterator to the last element of the container 'ctr'         |
    +--------------------------+------------------------------------------------------------------------+
    | ctr.rbegin()             | reverse begin (points to last element)                                 |
    +--------------------------+------------------------------------------------------------------------+
    | ctr.rend()               | reverse end (points to first element)                                  |
    +--------------------------+------------------------------------------------------------------------+
    | ctr.cbegin()             | returns a **constant** iterator to the first element of 'ctr'          |
    +--------------------------+------------------------------------------------------------------------+
    | ctr.cend()               | returns a **constant** iterator to the last element of 'ctr'           |
    +--------------------------+------------------------------------------------------------------------+
    | ctr.empty()              | return 'true' if 'ctr' is empty, otherwise 'false'                     |
    +--------------------------+------------------------------------------------------------------------+
    | ctr.size()               | return the number of elements in 'ctr'                                 |
    +--------------------------+------------------------------------------------------------------------+
    | ctr.insert(pos, val)     | insert value 'val' at position 'pos'                                   |
    +--------------------------+------------------------------------------------------------------------+
    | ctr.erase(itr1 [, itr2]) | erase element from position 'itr1' to 'itr2-1'.                        |
    +--------------------------+------------------------------------------------------------------------+
    | ctr.max_size()           | return maximum numbers of elements which can be inserted in 'ctr'      |
    +--------------------------+------------------------------------------------------------------------+
    | ctr1.swap(ctr2)          | swap the contents of 'ctr1' and 'ctr2'                                 |
    +--------------------------+------------------------------------------------------------------------+
    | swap(ctr1, ctr2)         | swap the contents of 'ctr1' and 'ctr2'                                 |
    +--------------------------+------------------------------------------------------------------------+
    | ctr.clear()              | delete all elements and set the size of container to 0 (not for array) |
    +--------------------------+------------------------------------------------------------------------+
    | ctr1 = ctr2              | copy the element of ctr2 in ctr1                                       |
    +--------------------------+------------------------------------------------------------------------+
    | ctr1 == ctr2,            | return 'true' if equal, otherwise 'false'                              |
    +--------------------------+------------------------------------------------------------------------+
    | ctr1 != ctr2             | return 'true' if not equal, otherwise 'false'                          |
    +--------------------------+------------------------------------------------------------------------+
    | ctr1 < ctr2 and others   | not applicable for unordered container                                 |
    +--------------------------+------------------------------------------------------------------------+



Vector operations
-----------------

Some of the possible vector operations are listed in :numref:`tbl_vec_op`, 

.. _`tbl_vec_op`:

.. table:: Vector operations

    +--------------------------+---------------------------------------------------------------+
    | Function                 | Description                                                   |
    +==========================+===============================================================+
    | **Declaration**          | Declaration and initialization of array 'v'                   |
    +--------------------------+---------------------------------------------------------------+
    | vector<eType> v          | create vector 'v' of size '0' and type 'eType'                |
    +--------------------------+---------------------------------------------------------------+
    | vector<eType> v(n)       | create vector 'v' of size 'n' (initialize with 0)             |
    +--------------------------+---------------------------------------------------------------+
    | vector<eType> v(n, val)  | create vector 'v' of size 'n' (initialize with 'val')         |
    +--------------------------+---------------------------------------------------------------+
    | vector<eType> v(v2)      | create a copy 'v' of vector 'v2'                              |
    +--------------------------+---------------------------------------------------------------+
    | vector<eType> v = v2     | create a copy 'v' of vector 'v2'                              |
    +--------------------------+---------------------------------------------------------------+
    | vector<eType> v = {1, 2} | create 'v' and initialize with values (1, 2)                  |
    +--------------------------+---------------------------------------------------------------+
    | **Assignment**           |                                                               |
    +--------------------------+---------------------------------------------------------------+
    | v1 = v2                  | copy v2 to v1                                                 |
    +--------------------------+---------------------------------------------------------------+
    | v.assign(n, val)         | assign 'n' elements of value 'val'                            |
    +--------------------------+---------------------------------------------------------------+
    | v1.swap(v2)              | swap the values of vector 'v1' and 'v2'                       |
    +--------------------------+---------------------------------------------------------------+
    | swap(v1, v2)             | swap the values of vector 'v1' and 'v2                        |
    +--------------------------+---------------------------------------------------------------+
    | **Access**               |                                                               |
    +--------------------------+---------------------------------------------------------------+
    | v[idx]                   | returns the element at 'idx' without range-check              |
    +--------------------------+---------------------------------------------------------------+
    | v.at(idx)                | return element at idx, and throws exception if out of range   |
    +--------------------------+---------------------------------------------------------------+
    | v.front()                | return first element (no check for existence of first element |
    +--------------------------+---------------------------------------------------------------+
    | v.back()                 | return last element (no check for existence of last element   |
    +--------------------------+---------------------------------------------------------------+
    | **Iterator**             |                                                               |
    +--------------------------+---------------------------------------------------------------+
    | v.begin()                | iterator to first element                                     |
    +--------------------------+---------------------------------------------------------------+
    | v.end()                  | iterator to last element                                      |
    +--------------------------+---------------------------------------------------------------+
    | v.cbegin()               | constant iterator to first element (i.e. read only)           |
    +--------------------------+---------------------------------------------------------------+
    | v.cend()                 | constant iterator to last element                             |
    +--------------------------+---------------------------------------------------------------+
    | v.rbegin()               | reverse begin (points to last element)                        |
    +--------------------------+---------------------------------------------------------------+
    | v.rend()                 | reverse end (points to first element)                         |
    +--------------------------+---------------------------------------------------------------+
    | v.crbegin()              | constant reverse iterator (points to last element)            |
    +--------------------------+---------------------------------------------------------------+
    | v.crend()                | constant reverse iterator (points to first element)           |
    +--------------------------+---------------------------------------------------------------+
    | **Checks**               |                                                               |
    +--------------------------+---------------------------------------------------------------+
    | v.empty()                | returns true if size=0                                        |
    +--------------------------+---------------------------------------------------------------+
    | v.size()                 | return size of the vector                                     |
    +--------------------------+---------------------------------------------------------------+
    | v.max_size()             | return maximum size of the vector                             |
    +--------------------------+---------------------------------------------------------------+
    | v.capacity()             | return maximum size without reallocation                      |
    +--------------------------+---------------------------------------------------------------+
    | v1 == v2, v1 !=v2 etc.   | returns true if condition is met                              |
    +--------------------------+---------------------------------------------------------------+
    | **Insert and remove**    |                                                               |
    +--------------------------+---------------------------------------------------------------+
    | v.push_back(val)         | append value to end                                           |
    +--------------------------+---------------------------------------------------------------+
    | v.pop_back()             | remove value from the end                                     |
    +--------------------------+---------------------------------------------------------------+
    | v.insert(pos, val)       | instert 'val' at 'pos'                                        |
    +--------------------------+---------------------------------------------------------------+
    | v.insert(pos, n, val)    | insert 'n' copies 'val' at 'pos'                              |
    +--------------------------+---------------------------------------------------------------+
    | v.erase(pos)             | remove element from 'pos'                                     |
    +--------------------------+---------------------------------------------------------------+
    | v.erase(itr1, itr2)      | delete element from location 'itr1' to 'itr2'                 |
    +--------------------------+---------------------------------------------------------------+
    | v.clear()                | remove all the elements.                                      |
    +--------------------------+---------------------------------------------------------------+

Some of the examples of vector operations are added below, 


.. code-block:: c++

    // vector_test.cpp

    #include <iostream>
    #include <vector>

    using namespace std;

    int main(){
        vector<int> v; // vector with no element 
        vector<int> v1 = {10, 12}; // 10 12
        vector<int> v2(v1); // 10 12
        vector<int> v3 = v1; // 10 12
        vector<int> v4(3); // initialize with zeros 0 0 0 
        vector<int> v5(3, 1); // 1 1 1

        for (auto i : v5)
            cout << i << " "; // 0, 0, 0
        cout << endl;

        cout << v1.size() << endl; // 2
        cout << v1.max_size() << endl; // 1073741823
        cout << v1.capacity() << endl; // 2

        return 0;
    }


Deque
-----

The operations in deque is same as vector operations (:numref:`tbl_vec_op`) with following changes, 

* 'capacity()' option is **not** available in 'deque'. 
* 'deque' allows insertion and deletion from the front as well using **push_front** and **pop_front** respectively. 


Arrays operations
-----------------


:numref:`tbl_array_dec_init` shows the various ways to declare and initialize the arrays. Further, :numref:`cpp_array_dec_init` shows the examples of the functions shown in :numref:`tbl_array_dec_init`. 

.. _`tbl_array_dec_init`:

.. table:: Array operations

    +----------------------------+-------------------------------------------------------------------+
    | Function                   | Description                                                       |
    +============================+===================================================================+
    | **Declaration**            | Declaration and initialization of array 'a'                       |
    +----------------------------+-------------------------------------------------------------------+
    | array<eType, N> a          | create array 'a' of size 'N' and type 'eType' with garbage values |
    +----------------------------+-------------------------------------------------------------------+
    | array<eType, N> a(a2)      | create a copy 'a' of array 'a2'                                   |
    +----------------------------+-------------------------------------------------------------------+
    | array<eType, N> a = a2     | create a copy 'a' of array 'a2'                                   |
    +----------------------------+-------------------------------------------------------------------+
    | array<eType, N> a = {1, 2} | create 'a' and initialize with values (1, 2, 0, 0 ...)            |
    +----------------------------+-------------------------------------------------------------------+
    | **Assignment**             |                                                                   |
    +----------------------------+-------------------------------------------------------------------+
    | a = a2                     | copy a2 to a                                                      |
    +----------------------------+-------------------------------------------------------------------+
    | a.fill(val)                | assign 'val' to each element in array 'a'                         |
    +----------------------------+-------------------------------------------------------------------+
    | a1.swap(a2)                | swap the values of array 'a1' and 'a2'                            |
    +----------------------------+-------------------------------------------------------------------+
    | swap(a1, a2)               | swap the values of array 'a1' and 'a2'                            |
    +----------------------------+-------------------------------------------------------------------+
    | **Access**                 |                                                                   |
    +----------------------------+-------------------------------------------------------------------+
    | a[idx]                     | returns the element at 'idx' without range-check                  |
    +----------------------------+-------------------------------------------------------------------+
    | a.at(idx)                  | return element at idx, and throws exception if out of range       |
    +----------------------------+-------------------------------------------------------------------+
    | a.front()                  | return first element (no check for existence of first element     |
    +----------------------------+-------------------------------------------------------------------+
    | a.back()                   | return last element (no check for existence of last element       |
    +----------------------------+-------------------------------------------------------------------+
    | **Iterator**               |                                                                   |
    +----------------------------+-------------------------------------------------------------------+
    | a.begin()                  | iterator to first element                                         |
    +----------------------------+-------------------------------------------------------------------+
    | a.end()                    | iterator to last element                                          |
    +----------------------------+-------------------------------------------------------------------+
    | a.cbegin()                 | constant iterator to first element (i.e. read only)               |
    +----------------------------+-------------------------------------------------------------------+
    | a.cend()                   | constant iterator to last element                                 |
    +----------------------------+-------------------------------------------------------------------+
    | a.rbegin()                 | reverse begin (points to last element)                            |
    +----------------------------+-------------------------------------------------------------------+
    | a.rend()                   | reverse end (points to first element)                             |
    +----------------------------+-------------------------------------------------------------------+
    | a.crbegin()                | constant reverse iterator (points to last element)                |
    +----------------------------+-------------------------------------------------------------------+
    | a.crend()                  | constant reverse iterator (points to first element)               |
    +----------------------------+-------------------------------------------------------------------+
    | **Checks**                 |                                                                   |
    +----------------------------+-------------------------------------------------------------------+
    | a.empty()                  | returns true if size=0                                            |
    +----------------------------+-------------------------------------------------------------------+
    | a.size()                   | return size of the array                                          |
    +----------------------------+-------------------------------------------------------------------+
    | a.max_size()               | return maximum size of the array                                  |
    +----------------------------+-------------------------------------------------------------------+
    | a1 == a2, a1 !=a2 etc.     | returns true if condition is met                                  |
    +----------------------------+-------------------------------------------------------------------+



.. code-block:: c++
    :caption: Some examples of Array operations
    :name: cpp_array_dec_init

    // array_dec_init.cpp

    #include <iostream>
    #include <array>

    using namespace std;

    int main(){
        array<int, 3> a; // garbage values 134514972 1 65535
        array<int, 3> a1 = {10, 12}; // 10 12 0
        array<int, 3> a2(a1); // 10 12 0
        array<int, 3> a3 = a1; // 10 12 0

        for (auto i : a)
            cout << i << " ";
        cout << endl;

        // size and max_size have same values for array
        cout << a1.size() << endl; // 3
        cout << a1.max_size() << endl; // 3

        return 0;
    }


More examples
=============

In previous sections, we saw the various elements of the 'standard template library' i.e. containers, iterators and algorithms. In this section, we will see some more examples along with some new features of each element. 



Iterating over the items
------------------------

As mentioned before, we should use the 'iterators' or 'range based for loops' for iterating over the elements in the containers. The :numref:`cpp_vector_iter` shows these two methods of iteration at Lines 19 and 28 respectively. 

.. note:: 

    Then 'range based for loop' is available in 'C++ 11', therefore we need to compile using 'C++ 11'

.. code-block:: c++
    :linenos:
    :emphasize-lines: 19, 28
    :caption: Correct ways of iterations
    :name: cpp_vector_iter

    // vector_iter.cpp 

    #include <iostream>
    #include <vector>
    #include <algorithm>

    using namespace std; 

    int main(){
        vector<int> v;
        v.push_back(2);
        v.push_back(-1);
        v.push_back(5);

        vector<int>:: iterator itr_begin = v.begin(); // stores start position
        vector<int>:: iterator itr_end = v.end(); // stores end position

        // print values in the container
        for (vector<int>:: iterator itr=itr_begin; itr!=itr_end; ++itr){
            cout << *itr << " ";  // 2 -1 5
        }
        cout << endl;

        // sort the values
        sort(itr_begin, itr_end); // sort values from itr_begin and itr_end

        // use 'range based for loop'
        for (int i : v)
            cout << i << " "; // -1 2 5
        cout << endl;

        return 0;
    }


.. code-block:: shell

    $ g++ -std=c++11 -o out vector_iter.cpp 
    $ ./out
    2 -1 5 
    -1 2 5 


Container's header
------------------

In :numref:`cpp_stl_headers`, all the headers of STL are added in Lines 3-14. Also, some features of vectors are shown i.e. 'at (Line 26)' 'clear (Line 29)', 'converting vector to array (Line 34)' and 'swap (Line 38)'. 

.. code-block:: c++
    :linenos:
    :emphasize-lines: 3-14, 26, 29, 38
    :caption: List of headers in STL
    :name: cpp_stl_headers

    // stl_headers.cpp

    #include <iostream>
    #include <vector>
    #include <deque>
    #include <list>
    #include <set> // set and multiset
    #include <unordered_set> // unordered set and multiset
    #include <map> // map and multimap 
    #include <unordered_map> // unordered map and multimap
    #include <iterator>
    #include <algorithm>
    #include <numeric> // numeric algorithms
    #include <functional> // functors

    using namespace std; 

    int main(){
        vector<int> v = {2, 4, 1}; 
        vector<int> w = {2, 4, 1}; 

        // size of the vector
        cout << "size = " << v.size() << endl; // size = 3, index start with 0

        // access the individual elements
        cout << "v[2] = " << v.at(2) << endl; // v[2] = 1
        cout << "v[2] = " << v[2] << endl; // v[2] = 1

        v.clear(); // clear the vector
        cout << "size = " << v.size() << endl; // size = 0
        
        // vectors are the 'dynamic contiguous array' 
        // save vector in array using pointer
        int* pw = &w[0];  // pointer to vector
        cout << "w[1] = " << pw[1] << endl; // w[1] = 4

        cout << "size (v, w): " << v.size() << ", " << w.size() << endl; // 0, 3
        v.swap(w); // save w in v; and v in w
        cout << "size after swap (v, w): " << v.size() << ", " << w.size() << endl; // 3, 0

        return 0;
    }


Below is the output of above code, 

.. code-block:: shell

    $ g++ -std=c++11 -o out stl_headers.cpp 
    $ ./out
    size = 3

    v[2] = 1
    v[2] = 1

    size = 0

    w[1] = 4

    size (v, w): 0, 3
    size after swap (v, w): 3, 0


List
----

In :numref:`cpp_link_list`, the insertion and deletion operation is performed on the double linked-list. Please see the location of iterator after 'insert (Line 25-26)', 'delete (Lines 31-34)' and 'slice (43-50)' operations. Read comments for more details.


.. code-block:: c++
    :linenos:
    :emphasize-lines: 25-26, 31-34, 36, 43-50
    :caption: Insertion and deletion in double link-list
    :name: cpp_link_list

    // link_list.cpp
    // double linked-list

    #include <iostream>
    #include <list>
    #include <algorithm>

    using namespace std;

    int main(){
        list<int> l = {3, 5, 2, 1, 10}, l2 = {0, 1};
        
        l.push_back(-2); // -2 will be added to end 
        l.push_front(14); // 14 will be added to front

        // print values in the list
        for (int i : l)
            cout << i << " "; // 14 3 5 2 1 10 -2 
        cout << endl;
        
        // find location of first 2
        list<int>::iterator itr = find(l.begin(), l.end(), 2);  // itr -> 2
        
        // insert 9 before 2
        l.insert(itr, 9); // 14 3 5 9 2 1 10 -2,
        cout << *itr << endl;  //                       itr -> 2

        ++itr; // increment the itr (or itr++)
        cout << *itr << endl;  //                       itr -> 1
        // delete item at location itr i.e. 1
        l.erase(itr); // 14 3 5 9 2 10 -2 

        // // itr is pointing to deleted item, therefore following line will generate error
        // cout << *itr << endl;  // itr -> 1, pointing to delete item '1'
        // itr++; // increment the itr (or itr++);         
        // cout << *itr << endl; // error

        // print values in the list
        for (int i : l)
            cout << i << " "; // 14 3 5 9 2 10 -2  
        cout << endl;

        // slice
        // locate 2 in l
        list<int>::iterator itr2 = find(l.begin(), l.end(), 2);  // itr -> 2
        // insert from '2 to end' of l, to the beginning of l2
        l2.splice(l2.begin(), l, itr2, l.end()); 
        for (int i : l2)
            cout << i << " "; // 2 10 -2 0 1 
        cout << endl;


        return 0;
    }


Set
---

In :numref:`cpp_set_op_ex`, a set (i.e. associative container) is used, which stored the unique values in sorted order. The insert operation return two types of values i.e. 'iterator' and 'boolean' (see Lines 22-28). Also, in associative containers, the elements can be erased by providing the values directly as shown in Line 30 (without using the iterators). 

.. code-block:: c++
    :linenos:
    :caption: Insertion and deletion in set
    :name: cpp_set_op_ex

    // set_op_ex.cpp

    #include <iostream>
    #include <set>

    using namespace std;

    int main(){
        set<int> s={4, 2, 2};
        for (int i : s)
            // automatically sorted and duplicate removed
            cout << i << " "; // 2 4 (not 4 2 2)
        cout << endl;

        s.insert(1); // 1 2 4  i.e. automatically sorted
        s.insert(12); // 1 2 4 12

        set<int>:: iterator itr; // iterator
        itr = s.find(4); // find 4
        cout << *itr << endl; // location of itr->4

        pair<set<int>::iterator, bool> ret; 
        ret = s.insert(2);

        // ret.first is the iterator 
        cout << "value entered: " << *ret.first << endl; // 2
        // ret.second => 0: False (value not inserted), 1: True
        cout << "value inserted: " << ret.second << endl; // 0 as already exist

        s.erase(2); // provide value of erase directly
        for (int i : s)
            cout << i << " "; // 1 4 12 
        cout << endl;

        return 0;
    }


Multimap
--------

Multimap can be used as dictionary as show in :numref:`cpp_map_op_ex`, where a dictionary of 'name' is created, 


.. code-block:: c++
    :linenos:
    :caption: Dictionary using  multimap
    :name: cpp_map_op_ex

    // map_op_ex.cpp

    #include <iostream>
    #include <string>
    #include <map>

    using namespace std; 

    int main(){
        multimap<string, string> m; // multimap

        // pair: type is defined explicitly
        m.insert (pair<string, string>("name", "Meher")); // pair
        
        // make_pair: type is inferred automatically
        m.insert (make_pair("name", "Krishna"));  // make_pair

        for ( auto i : m)
            cout << i.first << ": " << i.second << endl;


        return 0;
    }


Below is the output of above code, 

.. code-block:: shell

    $ g++ -std=c++11 -o out map_op_ex.cpp 
    $ ./out
    name: Meher
    name: Krishna
