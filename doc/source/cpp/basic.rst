.. _`ProgrammingBasics`:

First Program
*************

.. raw:: latex

    \chapterquote{No amount of prayer or meditation can do what helping others can do.}{Meher Baba}

Introduction
============

C language can be used in embedded design with Nios processor and SystemC. This tutorial contains various features of C language, which we will need for designing the SoPC (system on programmable chip) using NiosII processor. Further, the tutorials on 'SoPC design using C/C++' can be found at the `website <http://pythondsp.readthedocs.io/en/latest/pythondsp/toc.html>`_ under the Section 'VHDL/Verilog tutorials'.  In this chapter, simple 'Hello World' program is discussed along with it's execution. Further, the :numref:`tbl_KeywordsC` shows the list of Keywords which are discussed which are discussed in this tutorial. 


.. _`tbl_KeywordsC`:

.. table:: Keywords in C and C++

    +--------------+-------------+------------------+----------+
    | **C and C++**                                            |
    +==============+=============+==================+==========+
    | auto         | double      | int              | struct   |
    +--------------+-------------+------------------+----------+
    | break        | else        | long             | switch   |
    +--------------+-------------+------------------+----------+
    | case         | enum        | register         | typedef  |
    +--------------+-------------+------------------+----------+
    | char         | extern      | return           | unsigned |
    +--------------+-------------+------------------+----------+
    | const        | float       | short            | union    |
    +--------------+-------------+------------------+----------+
    | continue     | for         | sizeof           | while    |
    +--------------+-------------+------------------+----------+
    | default      | goto        | signed           | void     |
    +--------------+-------------+------------------+----------+
    | do           | if          | static           | volatile |
    +--------------+-------------+------------------+----------+
    | **Only C++**                                             |
    +--------------+-------------+------------------+----------+
    | asm          | FALSE       | public           | try      |
    +--------------+-------------+------------------+----------+
    | bool         | friend      | protected        | typeid   |
    +--------------+-------------+------------------+----------+
    | catch        | inline      | reinterpret_cast | typename |
    +--------------+-------------+------------------+----------+
    | class        | mutable     | static_cast      | using    |
    +--------------+-------------+------------------+----------+
    | const_cast   | namespace   | template         | virtual  |
    +--------------+-------------+------------------+----------+
    | delete       | new         | this             | wchar_t  |
    +--------------+-------------+------------------+----------+
    | dynamic_cast | operator    | throw            |          |
    +--------------+-------------+------------------+----------+
    | explicit     | private     | TRUE             |          |
    +--------------+-------------+------------------+----------+
    | **Bitwise (only C ++)**                                  |
    +--------------+-------------+------------------+----------+
    | and          | bitor       | not_eq           | xor      |
    +--------------+-------------+------------------+----------+
    | and_eq       | compl       | or               | xor_eq   |
    +--------------+-------------+------------------+----------+
    | bitand       | not         | or_eq            |          |
    +--------------+-------------+------------------+----------+

Installing Compiler
===================

We only need a C compiler to execute the programs. There are various C/C++ compiler available on Internet. Some of the C/C++ compilers are discussed below, so that we can compile both C and C++ codes. 

**Linux:** To install the compiler on Linux, type following command to the terminal, 

.. code-block:: shell

    $ sudo apt-get install g++

**Windows:** Install 'mingw' compiler and add it to windows path; or 'code\:block software' which contains the mingw compiler and set the mingw in the path after installation. Other options are also available e.g. Borland C or Turbo C etc.

Writing first code
==================

:numref:`c_helloWorld` shows the source code to print the 'Hello World' on the screen. To execute the code in the listing, open the terminal and go to the folder where program is saved and type following commands; which will generate the output on the terminal. 

.. code-block:: shell

    $ gcc -o out helloWorld.c 
    $ ./out                     (in Unix)
    $ out                       (in Windows)

**Explanation** :numref:`c_helloWorld`

    Note that, C is **case-sensitive** language. Each program contains one and only one  'main' function, which is the starting point of the code. In this listing, Line 6 has the 'main' function, which contains two other keywords i.e. 'int' and 'void'; here 'void' indicates that, the main 'function' does not have any input parameter, whereas 'int' represents that the return type of this function is 'integer'. 
    
    **Keywords** are nothing but predefined words in C, which can not be used for declaring **Identifiers** i.e. name of variables, functions and array etc. :numref:`tbl_KeywordsC` shows the list of keywords available in C and C++ languages. Further, the terms 'function', 'parameters' and 'return type' are discussed in :numref:`Chapter %s <ch_FunctionPointer>`. Further, all the statements inside the brackets (\{ \}) (i.e. between Line 7 and 11) are belongs to function 'main'. 

    In Line 9, keyword 'printf' is used, which prints the characters on the screen; this 'prinf' keyword is defied in 'standard input/output (i.e. stdio.h)' library, which is imported to the current program using Line 4. The '.h' files are known as 'header files' and C-programs are saved with extension '.c'. In the tutorial, all the file names are shown at the top of the code (See Line 1). 
    
    Note that,  multiple lines can be commented by using /\* and \*/ as shown in Lines 11-14; whereas '//' is used for single line comments (e.g. Line 1). The comments make code more readable; e.g. in this listing, functions of all the lines are described as comments.

    Also, it is the semicolon sign ';' which terminates the line, not the white spaces; e.g. Line 9 can be written in multiple lines as shown in Lines 11-14. If we uncomment these lines, then we will get the same output as Line 9. 
    
    Lastly, 'return 0 is used at Line 16, which means there is nothing to return. This line is required because we mention the return type as 'int' at Line 6. 

.. literalinclude:: codes/First-Program/helloWorld.c
    :language: c++
    :linenos:
    :caption: Print Hello World
    :name: c_helloWorld


Printing values in Binary, Hex, Octal and Decimal formats
=========================================================

In this section, we will learn to print the data in different formats i.e. Hexadecimal, Octal and Decimal. This can be done using identifiers '\%d (int)', '\%x (hex)' and '\%o (oct)' in the 'printf' statements, as shown in :numref:`c_hexDec`. For further understanding,  see the comments and outputs of the code in the listing. 

.. literalinclude:: codes/First-Program/hexDec.c
    :language: c++
    :linenos:
    :caption: Printing numbers in different formats
    :name: c_hexDec

Conclusion
==========

In this chapter, we see the list of compilers for C. Also, we wrote simple 'Hello World' programs, which illustrate the concept of 'main function', 'return' and 'printing output on screen'. Further, we learn to print the numeric-outputs in different formats as well. 



