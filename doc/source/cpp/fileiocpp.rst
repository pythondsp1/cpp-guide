.. _`ch_fileioCpp`:

File operations in C++
**********************

.. raw:: latex
    
    \chapterquote{But I say to you who listen: Love your enemies, do what is good to those who hate you, bless those who curse you, pray for those who mistreat you.}{Jesus Christ}

Write data to file
==================

In C++, for writing or reading data from a file, we need to include the 'fstream' header file as shown in Line 4 of :numref:`cpp_writeDataEx`. Next, we need to open the file, which is done at Line 13 using 'ofstream' and 'ios::out' keywords. This line opens the file as 'outFile' and if file does not open successfully, then 'if statement' at line 15 prints the message at Line 16. Note that, the file 'data.txt' file is saved inside the 'data' folder, therefore we need to create the 'data' folder first, as compiler can not create the folder. If file is open successfully, the compiler will reach to 'else' block at Line 19; where it will read the data from the terminal (Line 24), and then save it to file using Line 25. The 'while loop' is used at Line 24, which reads the data from terminal until 'end of file command' is given i.e. 'ctrl-Z' or 'crtl-C', as shown in :numref:`fig_writeDataEx`. Also, the data saved by the listing is shown in the :numref:`fig_writeDataEx2`. Lastly unlike C, in C++, file is automatically closed at the end of the program. 


.. literalinclude:: codes/File-operation-in-C++/writeDataEx.cpp
    :language: c++
    :linenos:
    :caption: Write data to file
    :name: cpp_writeDataEx


Read data from file
===================

Reading operation is similar to writing operation. The 'ifstream' and 'ios::in' keywords are used to read the data from the file, as shown in Line 13 :numref:`cpp_readDataEx`. 

.. literalinclude:: codes/File-operation-in-C++/readDataEx.cpp
    :language: c++
    :linenos:
    :caption: Read data from file
    :name: cpp_readDataEx


Conclusion
==========

In this chapter, we learn to read and write data to file using C++. We saw that the data can be stored  permanently in a file; and then this data can be processed and analyzed by some other software like Python and R etc. 